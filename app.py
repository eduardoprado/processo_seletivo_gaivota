from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
import json
import sqlalchemy as db
import pandas as pd
from tornado import gen
import os

class GetAll(RequestHandler):
    
  @gen.coroutine
  def get(self):
    global conn
    global weather
    query = db.select([weather])
    result = conn.execute(query).fetchall()
    df = pd.DataFrame(result)
    try:
        df.columns = result[0].keys()
    except:
        self.write({'message': 'The database is empty'})
    self.write(df.to_dict())


class RUD(RequestHandler):
    
  @gen.coroutine
  def post(self, _):
    global weather
    global conn
    query = db.insert(weather)
    value = json.loads(self.request.body)
    try:
      result = yield conn.execute(query,value)
      self.write({'message': 'Added'})
    except:
      self.write({'message': 'Error'})
    
  @gen.coroutine
  def delete(self, id):
    global weather
    global conn
    if id == 'all':
        query = db.delete(weather)
        message =  {'message': 'All rolls deleted'}
    else:
        query = db.delete(weather).where(weather.columns.Id == int(id))
        message =  {'message': 'Item with id %s was deleted' % id}
    conn.execute(query)
    self.write(message)


def test_delete(http_client):
  response = yield http_client.delete('http://localhost:3000/api/item/all')
  assert {'message': 'All rolls deleted'} == response
    
  
def make_app():
  urls = [
    ("/", GetAll),
    (r"/api/item/([^/]+)?", RUD)
  ]
  return Application(urls, debug=True)
  
if __name__ == '__main__':
  DB_IP = os.environ['DB_IP']
  engine = db.create_engine('postgresql://eduardo:eduardo@'+DB_IP+'/eduardo')
  conn = engine.connect()
  metadata = db.MetaData()
  weather = db.Table('weather', metadata, \
              db.Column('Id', db.Integer(), primary_key=True), \
              db.Column('date', db.String(255), nullable=False), \
              db.Column('precipitation', db.Float(), nullable=False), \
              db.Column('dry_bulb_temperature', db.Float(), nullable=False), \
              db.Column('wet_bulb_temperature', db.Float(), nullable=False), \
              db.Column('high_temperature', db.Float(), nullable=False), \
              db.Column('low_temperature', db.Float(), nullable=False), \
              db.Column('relative_humidity', db.Float(), nullable=False), \
              db.Column('relative_humidity_avg', db.Float(), nullable=False), \
              db.Column('pressure', db.Float(), nullable=False), \
              db.Column('wind_direction', db.Float(), nullable=False), \
              db.Column('wind_speed_avg', db.Float(), nullable=False), \
              db.Column('cloud_cover', db.Float(), nullable=False), \
              db.Column('evaporation', db.Float(), nullable=False), \
              db.Column('name_station', db.String(255), nullable=False))
  metadata.create_all(engine)
  app = make_app()
  app.listen(3000)
  IOLoop.instance().start()
