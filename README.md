
# Schema:

![image](gaivota.png)

# Requirements:

1. docker

2. docker-compose

# Instalation:

1. Go to the project's root folder

2. Type: `docker-compose up`

3. Type: `./url.sh`

4. Copy the url and paste at your browser

5. Run the project from jupyter interface


# Troubleshooting:

With not in Linux run the folling steps to get jupyter's url:

1. Type `docker container ps`

2. Find jupyter's ID and copy it

3. Type `docker exec $ID jupyter notebook list`

4. Copy url and paste at your browser


## Obs:
It's possible to test the API with curl:

1. Add row:

`curl -v -H "Content-Type: application/json" -X POST -d '{"date":"2020-01-01","precipitation":1.0,"dry_bulb_temperature":1.0,"wet_bulb_temperature":1.0,"high_temperature":1.0,"low_temperature":1.0,"relative_humidity":1.0,"relative_humidity_avg":1.0,"pressure":1.0,"wind_direction":1.0,"wind_speed_avg":1.0,"cloud_cover":1.0,"evaporation":1.0,"name_station":"name_station"}' http://localhost:3000/api/item/`


2. Delete item:

`curl -X DELETE http://localhost:3000/api/item/ID`

3. Delete all:

`curl -X DELETE http://localhost:3000/api/item/all`

2. It's possible get all rows in browser:

`http://localhost:3000/`

